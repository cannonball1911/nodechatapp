const expect = require('expect')

const {Rooms} = require('./rooms.js')

describe('Rooms', () => {
    let rooms

    beforeEach(() => {
        rooms = new Rooms()
        rooms.rooms = ['test', 'test room 1', 'test room 1']
    })

    it('should add new room', () => {
        const rooms = new Rooms()
        const roomName = 'test123'
        const resRoom = rooms.addRoom(roomName)

        expect(resRoom).toBe(roomName)
        expect(rooms.rooms).toEqual([roomName])
    })

    it('should remove a room', () => {
        const roomName = 'test room 1'
        const resRoom = rooms.removeRoom(roomName)

        expect(resRoom).toBe(roomName)
        expect(rooms.rooms.length).toBe(2)
    })

    it('should not remove a room', () => {
        const roomName = 'wut is this'
        const resRoom = rooms.removeRoom(roomName)

        expect(resRoom).toBeFalsy()
        expect(rooms.rooms.length).toBe(3)
    })

    it('should find room', () => {
        const roomName = 'test'
        const resRoom = rooms.getRoom(roomName)

        expect(resRoom).toBe(roomName)
    })

    it('should not find room', () => {
        const roomName = 'classy room'
        const resRoom = rooms.getRoom(roomName)

        expect(resRoom).toBeFalsy()
    })
})