const expect = require('expect')

const {Users} = require('./users.js')

describe('Users', () => {
    let users

    beforeEach(() => {
        users = new Users()
        users.users = [{
            id: '1',
            name: 'Kai',
            room: 'Node Course'
        }, {
            id: '2',
            name: 'Fritz',
            room: 'React Course'
        }, {
            id: '3',
            name: 'Adolf',
            room: 'Node Course'
        }]
    })

    it('should add new user', () => {
        const users = new Users()
        const user = {
            id: '123',
            name: 'Kai',
            room: 'Office'
        }
        const resUser = users.addUser(user.id, user.name, user.room)

        expect(users.users).toEqual([user])
    })

    it('should remove a user', () => {
        const userID = '1'
        const user = users.removeUser(userID)

        expect(user.id).toBe(userID)
        expect(users.users.length).toBe(2)
    })

    it('should not remove a user', () => {
        const userID = '999'
        const user = users.removeUser(userID)

        expect(user).toBeFalsy()
        expect(users.users.length).toBe(3)
    })

    it('should find user', () => {
        const userID = '2'
        const user = users.getUser(userID)

        expect(user.id).toBe(userID)
    })

    it('should not find user', () => {
        const userID = '999'
        const user = users.getUser(userID)

        expect(user).toBeFalsy()
    })

    it('should return names for node course', () => {
        const userList = users.getUserList('Node Course')

        expect(userList).toEqual(['Kai', 'Adolf'])
    })

    it('should return names for react course', () => {
        const userList = users.getUserList('React Course')

        expect(userList).toEqual(['Fritz'])
    })
})