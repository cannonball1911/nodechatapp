class Rooms {
    constructor () {
        this.rooms = []
    }

    addRoom (roomName) {
        this.rooms.push(roomName)
        return roomName
    }

    removeRoom (roomName) {
        const room = this.getRoom(roomName)

        if (room) {
            this.rooms.splice(this.rooms.indexOf(roomName), 1)
        }

        return room
    }

    getRoom (roomName) {
        return this.rooms.find((element) => {
            return element === roomName
        })
    }
}

module.exports = {Rooms}