const expect = require('expect')

const {generateMessage, generateLocationMessage} = require('./message.js')

describe('generateMessage', () => {
    it('should generate correct message object', () => {
        const from = 'Kai'
        const text = 'Some message'
        const message = generateMessage(from, text)

        expect(typeof message.createdAt === 'number')
        expect(message).toMatchObject({from, text})
    })
})

describe('generateLocationMessage', () => {
    it('should generate correct location object', () => {
        const from = 'Kai'
        const latitude = 1
        const longitude = 1
        const url = `https://www.google.com/maps?q=${latitude},${longitude}`
        const message = generateLocationMessage(from, latitude, longitude)

        expect(typeof message.createdAt === 'number')
        expect(message).toMatchObject({from, url})
    })
})