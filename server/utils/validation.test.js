const expect = require('expect')

const {isRealString} = require('./validation.js')

describe('isRealString', () => {
    it('should reject non-string values', () => {
        const res = isRealString(5)

        expect(res).toBeFalsy()
    })

    it('should reject string with only spaces', () => {
        const res = isRealString('     ')

        expect(res).toBeFalsy()
    })

    it('should allow string with non-space characters', () => {
        const res = isRealString('  Max Mustermann ')
        
        expect(res).toBeTruthy()
    })
})