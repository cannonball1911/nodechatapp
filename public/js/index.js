let socket = io()

socket.on('updateRoomDropdown', function (rooms) {
    const template = jQuery('#roomDropdown-option-template').html()

    rooms.forEach(function (room) {
        const html = Mustache.render(template, {
            option: room
        })

        jQuery('#roomDropdown').append(html)
    })
})